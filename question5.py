import os
import shutil

def createfile():
    num = 0
    while num < 100:
        num += 1
        path = r'C:\Users\rgollapudi\Documents\python\sep1\source'
        filename = str(num)+'.txt'
        with open(os.path.join(path, filename), 'w') as fp:
            fp.write('test file')

def copyfiles():
    source = 'C:\\Users\\rgollapudi\\Documents\\python\\sep1\\source'
    destination = 'C:\\Users\\rgollapudi\\Documents\\python\\sep1\\destination'
    prefix = input("prefix : ")
    suffix = int(input("start nums: "))

    for name in os.listdir(source):
        sfile = os.path.join(source,name)
        dfile = os.path.join(destination,prefix+str(suffix)+".txt")
        suffix += 1
        shutil.copy(sfile,dfile)

createfile()
copyfiles()
