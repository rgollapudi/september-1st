def merging(d1, d2):
	for val in d2.keys():
		d1[val]=d2[val]
	return d1
#first dictionary	
d1 = {'a': 1, 'b': 2}
#second dictionary
d2 = {'c': 3, 'd': 4}
# calling merging function
d3 = merging(d1, d2)
#print the resultant dictionary
print(d3)
