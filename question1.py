# creating a node for taking instance objects
class node:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
"""function to point starting and next values """
def list(elements):
	head = node(elements[0])
	for element in elements[1:]:
		ptr = head
		while ptr.next:
			ptr = ptr.next
		ptr.next = node(element)
	return head
"""function to print the list"""
def printlist(head):
	ptr = head
	print('[', end = "")
	while ptr:
		print(ptr.val, end = ", ")
		ptr = ptr.next
	print(']')
# logic to build addition of two numbers in a list
class Solution:
	def addTwoNumbers(self, l1:node, l2: node) -> node:
		n1 = ""
		n2 = ""
		while l1 != None:
			n1 += str(l1.val)
			l1 = l1.next
		while l2 != None:
			n2 += str(l2.val)
			l2 = l2.next
		res = str(int(n1)+ int(n2))
		nav = flag = node(res[0])
		for e in res:
			nav.next = node(e)
			nav = nav.next
		return flag.next
# driver code
x = Solution()
list1 = list([4,3,2])
list2 = list([5,1,4])
printlist(x.addTwoNumbers(list1, list2))
