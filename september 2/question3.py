openbrackets = ["[","{","("]
closedbrackets = ["]","}",")"]

# Function to check parentheses
def isbalanced(input):
	stack = []
	for i in input:
		if i in openbrackets:
			stack.append(i)
		elif i in closedbrackets:
			pos = closedbrackets.index(i)
			if ((len(stack) > 0) and
				(openbrackets[pos] == stack[len(stack)-1])):
				stack.pop()
			else:
				return "Unbalanced"
	if len(stack) == 0:
		return "Balanced"
	else:
		return "Unbalanced"


filename=input()
textfile = open(filename, "r")
data = textfile.read()
textfile.close()

input = str(data)
print(input,"-", isbalanced(input))

