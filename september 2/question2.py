# creating class node  
class Node:
  """function to initialise node object"""
  def __init__(self,data):
    self.data = data
    self.next = None
# create an instance object for head node
class Linkedlist:
  """function to initialise linkedlist object"""
  def __init__(self):
    self.head = None 
# logic for inserting node at beginning  
  def insertatbeg(self,data):
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
#  logic for inserting node at end 
  def insertatend(self,data):
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
# logic for inserting node at particular position   
  def insertatposition(self,pos,data):
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
# logic for deleting node at beginning  
  def deleteatbeg(self):
    temp = self.head 
    self.head = temp.next 
    temp.next = None
# logic for deleting node at end  
  def deleteatend(self):
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
# logic for deleting node at particular position  
  def deleteatposition(self,pos):
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
#  displaying the data in linkedlist  
  def display(self):
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      new = ""
      while(temp):
        new+=temp.data
        temp=temp.next 
      return new
# check if expression is balanced or not
def is_balanced(expr):
	stack = []

	# Traverse the Expression
	for char in expr:
		if char in ["(", "{", "["]:
			stack.append(char)
		else:

			if not stack:
				return False
			current_char = stack.pop()
			if current_char == '(':
				if char != ")":
					return False
			if current_char == '{':
				if char != "}":
					return False
			if current_char == '[':
				if char != "]":
					return False
	if stack:
		return False
	return True

l = Linkedlist()
n = Node('(')
l.head = n
n1 = Node('{')
n.next = n1 
n2 = Node('}')
n1.next = n2 
# n3 = Node(')')
# n2.next = n3 
# l.insert_at_begining('[')
l.insertatend(')')
# l.delete_at_end()
x = l.display()

exp = x
print(exp)

#call the function
if is_balanced(exp):
	print("Balanced")
else:
	print("Not Balanced")
