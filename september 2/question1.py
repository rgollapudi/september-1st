# creating class node  
class Node:
  """function to initialise node object"""
  def __init__(self,data):
    self.data = data
    self.next = None
# create an instance object for head node
class Linkedlist:
  """function to initialise linkedlist object"""
  def __init__(self):
    self.head = None 
# logic for inserting node at beginning
  def insertAtFirst(self,data):
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
#  logic for inserting node at end
  def insertAtEnd(self,data):
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
# logic for inserting node at particular position 
  def insertAtPos(self,pos,data):
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
# logic for deleting node at beginning 
  def deleteAtStart(self):
    temp = self.head 
    self.head = temp.next 
    temp.next = None
# logic for deleting node at end 
  def deleteAtEnd(self):
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
# logic for deleting node at particular position 
  def deleteAtPos(self,pos):
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
#  displaying the data in linkedlist 
  def display(self):
    if self.head is None:
      print("null")
    else:
      temp = self.head 
      while(temp):
        print(temp.data , end = " ")
        temp=temp.next 
# driver code
l = Linkedlist()
n = Node(1)
l.head = n
n1 = Node(2)
n.next = n1 
n2 = Node(3)
n1.next = n2 
n3 = Node(4)
n2.next = n3 
l.insertAtFirst(5)
l.insertAtEnd(5)
l.insertAtPos(1,5)
l.deleteAtStart()
l.deleteAtEnd()
l.deleteAtPos(1)
l.display()
