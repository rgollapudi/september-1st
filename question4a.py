import operator
#create dictionary
newdict = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
#sorting dict by value using sort method
ascending = dict(sorted(newdict.items(), key=operator.itemgetter(1)))
print('Ascending order by value : ',ascending)
#put reverse as true for descending order  
descending = dict( sorted(newdict.items(), key=operator.itemgetter(1),reverse=True))
print('Descending order by value : ',descending)
