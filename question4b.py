import operator
#create a dictionary
newdict = {1: 7, 3: 3, 4: 5, 2: 8, 0: 0}
#sorting items
asc = dict(sorted(newdict.items(), key=operator.itemgetter(0)))
print('Ascending order by key : ',asc)
#reversing the order  
desc = dict( sorted(newdict.items(), key=operator.itemgetter(0),reverse=True))
print('Descending order by key : ',desc)
